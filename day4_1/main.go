package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

type passport struct {
	byr, iyr, eyr, hgt, hcl, ecl, pid, cid string
}

func (p passport) valid() bool {
	return p.byr != "" &&
		p.iyr != "" &&
		p.eyr != "" &&
		p.hgt != "" &&
		p.hcl != "" &&
		p.ecl != "" &&
		p.pid != ""
}

func newPassport(in string) passport {
	fields := strings.Split(in, " ")
	m := make(map[string]string)
	for _, v := range fields {
		kv := strings.Split(v, ":")
		m[kv[0]] = kv[1]
	}
	return passport{
		byr: m["byr"],
		iyr: m["iyr"],
		eyr: m["eyr"],
		hgt: m["hgt"],
		hcl: m["hcl"],
		ecl: m["ecl"],
		pid: m["pid"],
		cid: m["cid"],
	}
}

func main() {
	input := getInput(openInput())
	res := solve(input)

	fmt.Println(res)
}

func solve(input []string) int {
	var res int
	for _, v := range input {
		p := newPassport(v)
		if p.valid() {
			res++
		}
	}
	return res
}

func openInput() io.Reader {
	file, err := os.Open("./input.txt")
	panicOnErr(err)
	return file
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

func getInput(reader io.Reader) []string {
	input, err := ioutil.ReadAll(reader)
	panicOnErr(err)
	blocks := strings.Split(string(input), "\n\n")
	res := make([]string, 0, 100)
	for _, v := range blocks {
		res = append(res, strings.Replace(v, "\n", " ", -1))
	}
	return res
}

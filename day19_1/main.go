package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func main() {
	input := getInput(openInput())
	fmt.Println(input)
}

func openInput() io.Reader{
	file, err := os.Open("./input.txt")
	panicOnErr(err)
	return file
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

func getInput(reader io.Reader) []int{
	scanner := bufio.NewScanner(reader)
	res := make([]int, 0, 100)
	for scanner.Scan() {
		num, err := strconv.Atoi(scanner.Text())
		panicOnErr(err)
		res = append(res, num)
	}
	return res
}
package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type passport struct {
	byr, iyr, eyr, hgt, hcl, ecl, pid, cid string
}

func hgtValid(hgt int, hgtType string) bool {
	if hgtType == "in" {
		return hgt >= 59 && hgt <= 76
	} else {
		return hgt >= 150 && hgt <= 193
	}
}

func (p passport) valid() bool {
	if p.hgt == "" {
		return false
	}
	byr, _ := strconv.Atoi(p.byr)
	iyr, _ := strconv.Atoi(p.iyr)
	eyr, _ := strconv.Atoi(p.eyr)
	hgt, _ := strconv.Atoi(p.hgt[:len(p.hgt)-2])
	hgtType := p.hgt[len(p.hgt)-2:]
	b, _ := regexp.MatchString("^#([a-fA-F0-9]{6})", p.hcl)
	return p.byr != "" && byr >= 1920 && byr <= 2002 &&
		p.iyr != "" && iyr >= 2010 && iyr <= 2020 &&
		p.eyr != "" && eyr >= 2020 && eyr <= 2030 &&
		p.hgt != "" && hgtValid(hgt, hgtType) &&
		p.hcl != "" && b &&
		(p.ecl == "amb" || p.ecl == "blu" || p.ecl == "brn" || p.ecl == "gry" || p.ecl == "grn" || p.ecl == "hzl" || p.ecl == "oth") &&
		p.pid != "" && len(p.pid) == 9
}

func newPassport(in string) passport {
	fields := strings.Split(in, " ")
	m := make(map[string]string)
	for _, v := range fields {
		kv := strings.Split(v, ":")
		m[kv[0]] = kv[1]
	}
	return passport{
		byr: m["byr"],
		iyr: m["iyr"],
		eyr: m["eyr"],
		hgt: m["hgt"],
		hcl: m["hcl"],
		ecl: m["ecl"],
		pid: m["pid"],
		cid: m["cid"],
	}
}

func main() {
	input := getInput(openInput())
	res := solve(input)

	fmt.Println(res)
}

func solve(input []string) int {
	var res int
	for _, v := range input {
		p := newPassport(v)
		if p.valid() {
			res++
		}
	}
	return res
}

func openInput() io.Reader {
	file, err := os.Open("./input.txt")
	panicOnErr(err)
	return file
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

func getInput(reader io.Reader) []string {
	input, err := ioutil.ReadAll(reader)
	panicOnErr(err)
	blocks := strings.Split(string(input), "\n\n")
	res := make([]string, 0, 100)
	for _, v := range blocks {
		res = append(res, strings.Replace(v, "\n", " ", -1))
	}
	return res
}

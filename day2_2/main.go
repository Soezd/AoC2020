package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Password struct {
	lowCount  int
	highCount int
	character rune
	password  string
}

func (p Password) check() bool {
	valid := false
	if p.password[p.lowCount-1] == byte(p.character) {
		valid = !valid
	}
	if p.password[p.highCount-1] == byte(p.character) {
		valid = !valid
	}
	return valid
}

func main() {
	input := getInput(openInput())
	solve(input)
}

func solve(passwords []Password) {
	valid := 0
	for _, v := range passwords {
		if v.check() {
			valid++
		}
	}
	fmt.Println(valid)
}

func openInput() io.Reader {
	file, err := os.Open("./input.txt")
	panicOnErr(err)
	return file
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

func getInput(reader io.Reader) []Password {
	scanner := bufio.NewScanner(reader)
	res := make([]Password, 0, 100)
	for scanner.Scan() {
		content := scanner.Text()
		password := parsePassword(content)
		res = append(res, password)
	}
	return res
}

func parsePassword(in string) Password {
	s := strings.Split(in, ":")
	rules := s[0]
	password := s[1]
	s = strings.Split(rules, " ")
	constraints := s[0]
	char := s[1][0]
	numbers := strings.Split(constraints, "-")
	min, err := strconv.Atoi(numbers[0])
	panicOnErr(err)
	max, err := strconv.Atoi(numbers[1])
	panicOnErr(err)
	return Password{
		min,
		max,
		rune(char),
		strings.TrimSpace(password),
	}
}

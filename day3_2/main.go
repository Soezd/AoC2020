package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	input := getInput(openInput())
	res := solve(input)
	fmt.Println(res)
}

func solve(input []string) int {
	width := len(input[0])
	res := slope(input, width, 1, 1)
	res *= slope(input, width, 3, 1)
	res *= slope(input, width, 5, 1)
	res *= slope(input, width, 7, 1)
	res *= slope(input, width, 1, 2)
	return res
}

func slope(input []string, width, right, down int) int {
	counter := 0
	count := 0
	for i, v := range input {
		if i == 0 || i%down != 0 {
			continue
		}
		counter += right
		counter %= width
		if v[counter] == byte('#') {
			count++
		}
	}
	return count
}

func openInput() io.Reader {
	file, err := os.Open("./input.txt")
	panicOnErr(err)
	return file
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

func getInput(reader io.Reader) []string {
	scanner := bufio.NewScanner(reader)
	res := make([]string, 0, 100)
	for scanner.Scan() {
		res = append(res, strings.TrimSpace(scanner.Text()))
	}
	return res
}

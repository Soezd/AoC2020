package main

import "testing"

func Test_solve(t *testing.T) {
	type args struct {
		input []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test AoC",
			args: args{
				input: []string{
					"..##.......",
					"#...#...#..",
					".#....#..#.",
					"..#.#...#.#",
					".#...##..#.",
					"..#.##.....",
					".#.#.#....#",
					".#........#",
					"#.##...#...",
					"#...##....#",
					".#..#...#.#",
				},
			},
			want: 336,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve(tt.args.input); got != tt.want {
				t.Errorf("solve() = %v, want %v", got, tt.want)
			}
		})
	}
}

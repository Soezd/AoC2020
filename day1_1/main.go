package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

const TARGET = 2020 

func main() {
	input := getInput(openInput())
	solve(input)
}

func solve(input []int){
	for i, v := range input {
		needle := TARGET - v
		for ii, val := range input[i+1:]{
			if val == needle {
				fmt.Printf("First: Index: %v, Value: %v, Second: Index: %v, Value: %v\n", ii, val, i, v)
				fmt.Printf("Product: %v\n", v*val)
			}
		}
	} 
}

func openInput() io.Reader{
	file, err := os.Open("./input.txt")
	panicOnErr(err)
	return file
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

func getInput(reader io.Reader) []int{
	scanner := bufio.NewScanner(reader)
	res := make([]int, 0, 100)
	for scanner.Scan() {
		num, err := strconv.Atoi(scanner.Text())
		panicOnErr(err)
		res = append(res, num)
	}
	return res
}